-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 10, 2013 at 03:53 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `addressbook`
--

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `name_id` int(11) NOT NULL,
  `email` varchar(40) NOT NULL,
  PRIMARY KEY (`name_id`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`name_id`, `email`) VALUES
(2, 'john@demo.com'),
(2, 'john@example.com'),
(2, 'throwaway@demo.com'),
(3, 'baz@hotmail.com'),
(3, 'jane@example.com'),
(11, 'baz@foo.com'),
(12, 'monkey@thedoor.com'),
(12, 'monkey@thezoo.com'),
(13, 'test@tester.com');

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `name_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`name_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`name_id`, `name`) VALUES
(2, 'john doe'),
(3, 'jane doe'),
(4, 'ted'),
(11, 'baz'),
(12, 'monkey'),
(13, 'timmy'),
(14, 'timmy');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `emails`
--
ALTER TABLE `emails`
  ADD CONSTRAINT `emails_ibfk_1` FOREIGN KEY (`name_id`) REFERENCES `people` (`name_id`) ON DELETE CASCADE;
