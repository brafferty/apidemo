<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'giiPassword',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
                            // REST patterns - allow named parameters as /parameterName/parameterValue...
                                array('api/list', 'pattern'=>'api/list/<id:\d+>/*', 'verb'=>'GET'),
                                array('api/viewPerson', 'pattern'=>'api/person/<id:\d+>/*', 'verb'=>'GET'),
                                array('api/updatePerson', 'pattern'=>'api/person/<id:\d+>/*', 'verb'=>'POST'),
                                array('api/viewEmail', 'pattern'=>'api/email/<id:\d+>/*', 'verb'=>'GET'),
                                array('api/updateEmail', 'pattern'=>'api/email/<id:\d+>/*', 'verb'=>'POST'),
                                array('api/addEmail', 'pattern'=>'api/email/new<id:\d+>/*', 'verb'=>'POST'),
                                array('api/deleteEmail', 'pattern'=>'api/email/<id:\d+>/*', 'verb'=>'DELETE'),
                                array('api/deletePerson', 'pattern'=>'api/email/<id:\d+>/*', 'verb'=>'DELETE'),
                                array('api/createPerson', 'pattern'=>'api/create<model:\w+>', 'verb'=>'POST'),

                            
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
            
            /*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
               */
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=addressbook',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);