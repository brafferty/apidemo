<?php

    /*
     * Need to match the name_id with a valid name_id from the people table
     * Name -> ID
     * Tom  -> 1
     * Bob  -> 2
     * Sam  -> 3
     */

    return array(
            'email1'=>array(
              'email' => 'tom@hotmail.com',
              'name_id' => '1',
            ),
            'email2'=>array(
              'email' => 'tom@gmail.com',
               'name_id' => '1',
            ),
        
            'email3'=>array(
              'email' => 'bob@yahoo.com',
              'name_id' => '2',
            ),

            'email4'=>array(
              'email' => 'bob@eircom.net',
              'name_id' => '2',
            ),
        
            'email5'=>array(
              'email' => 'sam@home.com',
              'name_id' => '3',
            ),
        )
?>
