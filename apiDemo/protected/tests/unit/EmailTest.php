<?php

    class EmailTest extends CDbTestCase
       {
        
            public $fixtures=array
            (
                'people'=>'People',
                'emails'=>'Emails',
            );

            
           public function testEmailCreate(){
               //Create a new person
                $newEmail=new Emails;
                $newEmailAddress = 'thomas@tcd.ie';
                
                //need to check the name_id is in teh People table
                $nameId = '1'; //change to ensure test fails for non valid name_id
                $checkNameId=People::model()->findByPk($nameId);
                $this->assertTrue($checkNameId instanceof People);
                
                
                $newEmail->setAttributes( array(
                                'email' => $newEmailAddress,
                                'name_id' => $nameId,
                            )
                );
                $this->assertTrue($newEmail->save(false));
                 
                //Read back the newly created email
                $retrievedEmail=Emails::model()->findByAttributes(array('email'=>$newEmailAddress));
                $this->assertTrue($retrievedEmail instanceof Emails);
                $this->assertEquals($newEmailAddress,$retrievedEmail->email);
                
            }
            
            public function testEmailRead(){
                $retrievedEmail = $this->emails('email1');
                $this->assertTrue($retrievedEmail instanceof Emails);
                $this->assertEquals('tom@hotmail.com',$retrievedEmail->email);
            }
          
            
           public function testEmailUpdate(){
                $email = $this->emails('email2');
                $updatedEmailAddress = 'Harry@gmail.com';
                $email->email = $updatedEmailAddress;
                $this->assertTrue($email->save(false));
                //read back the record again to ensure the update worked
                $updatedEmail=Emails::model()->findByAttributes(array('email'=>$email->email));
                $this->assertTrue($updatedEmail instanceof Emails);
                $this->assertEquals($updatedEmailAddress,$updatedEmail->email);
            }
            
            public function testEmailDelete(){
                $email = $this->emails('email3');
                $savedEmailAddress = $email->email;
                $this->assertTrue($email->delete());
                $deletedEmail=Emails::model()->findByAttributes(array('email'=>$savedEmailAddress));
                $this->assertEquals(NULL,$deletedEmail);
            }
            
            
            
            
       }//end class           
?>
