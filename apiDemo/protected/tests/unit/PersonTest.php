<?php

/*
 * Test the CRUD operations on the People table
 */
    class PersonTest extends CDbTestCase
       {
        
            public $fixtures=array
            (
                'people'=>'People',
                'emails'=>'Emails',
            );
        
            /*
             * the name_id column auto increments so can be left blank
             */
           public function testPersonCreate()
           {
               //Create a new person
                $newPerson=new People;
                $newPersonName = 'Bilbo';
                $newPerson->setAttributes(
                            array(
                                'name' => $newPersonName,
                            )
                );
                $this->assertTrue($newPerson->save(false));
                 
                //Read back the newly created person
                $retrievedPerson=People::model()->findByPk($newPerson->name_id);
                $this->assertTrue($retrievedPerson instanceof People);
                $this->assertEquals($newPersonName,$retrievedPerson->name);
                
            }
            
            
            public function testPersonRead(){
                $retrievedPerson = $this->people('person1');
                $this->assertTrue($retrievedPerson instanceof People);
                $this->assertEquals('Tom',$retrievedPerson->name);
            }
            
            
            /*
             * test update of person name - doesnt't affect name_id
             */
            public function testPersonUpdate(){
                $person = $this->people('person2');
                $updatedPersonName = 'Bill';
                $person->name = $updatedPersonName;
                $this->assertTrue($person->save(false));
                //read back the record again to ensure the update worked
                $updatedPerson=People::model()->findByPk($person->name_id);
                $this->assertTrue($updatedPerson instanceof People);
                $this->assertEquals($updatedPersonName,$updatedPerson->name);
            }
            
            /*
             * Need to ensure that when a person is deleted all of their associated
             * emails are  deleted
             * This should happen automatically due to table constraints but check anyway!
             */
            public function testPersonDelete(){
                $person = $this->people('person2');
                $savedPersonId = $person->name_id;
                $this->assertTrue($person->delete());
                
                //make sure the name_id is gone from the People table
                $deletedPerson=People::model()->findByPk($savedPersonId);
                $this->assertEquals(NULL,$deletedPerson);
                
                //check the name_id is gone from the emails table
                $deletedPersonEmail=Emails::model()->findByAttributes(array('name_id'=>$savedPersonId));
                $this->assertEquals(NULL,$deletedPersonEmail);
                
          }

          
          
          
       }//end class

?>
