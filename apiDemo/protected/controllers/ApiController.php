<?php

class ApiController extends Controller
{
	/**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';
    
    /**
     * @return array action filters
     */
    public function filters()
    {
            return array();
    }
 
    //just a tester for the page
    public function actionIndex(){
           echo CJSON::encode(array(1, 2, 3));
           
    }
    
    // Actions
    
    /*
     * Easy option to view contents of either people table or emails table.
     * admitedly very similar to the example on the yii wiki page but it simply
     * returns the contents of the selected database table in json format 
     */
    public function actionList()
    {
        switch($_GET['model'])
        {
        	case 'people':
                    $models = People::model()->findAll();
                    break;
                case 'emails':
                    $models = Emails::model()->findAll();
                    break;
                default:
                $this->_sendResponse(501, "Model can either be 'people' or 'emails'." );
        }
        if(!empty($models)) {
            $rows = array();
            foreach($models as $model){
                $rows[] = $model->attributes;
            }

            $this->_sendResponse(200, CJSON::encode($rows));
        } else {
            $this->_sendResponse(200, sprintf('No items where found for model <b>%s</b>', $_GET['model']) );
        }
    }
    
    /*
     * view all emails associated with a person
     */
    public function actionViewPerson()
    {
        if(!isset($_GET['name_id'])){
            $this->_sendResponse(500, 'Error: Parameter <b>name_id</b> is missing' );
        }
        
    	$person=People::model()->findByPk($_GET['name_id']);
    	if(!empty($person))
    	{
            $email_addresses = Emails::model()->findAllByAttributes(array('name_id'=>$_GET['name_id']));
            if(!empty($email_addresses)){
                $rows = array();
                foreach($email_addresses as $email){
                    $rows[] = $email->email;
                }

                $this->_sendResponse(200, CJSON::encode($rows));
            }
                
            else{
                $this->_sendResponse(406, sprintf('Email addresses associated with <b>%s</b> 
                                    could not be found', $_GET['email']));
            }
            
    	}
    	else 
    	{
    		$this->_sendResponse(406, 'Person data not valid');
    	}
    }
    
    /*
     * view the person associated with an email address
     */
    public function actionViewEmail(){
        if(!isset($_GET['email'])){
            $this->_sendResponse(500, 'Error: Parameter <b>email</b> is missing' );
        }
        
        $email_address =Emails::model()->findByAttributes(array('email'=>$_GET['email']));
    	if(!empty($email_address))
    	{
            $name_id = $email_address->name_id;
            $person = People::model()->findByPk($name_id);
            if(!empty($person)){
                $this->_sendResponse(200, CJSON::encode($person));
            }
            else{
                $this->_sendResponse(406, sprintf('Person associated with <b>%s</b> 
                                    could not be found', $_GET['email']));
            }
    	}
    	else 
    	{
    		$this->_sendResponse(406, 'Email address could not be found');
    	}
    }
    
    
    /*
     * create BOTH a new person and email in the address book
     * this WILL allow for the same name to be entered more than once
     * hence the need for the name_id in the people table
     * another assumption that I'm making it that there is no need to be able to 
     * create just a new person without an email address 
     */
    public function actionCreatePerson()
    {
        if(!isset($_POST['name'])){
            $this->_sendResponse(500, 'Error: Parameter <b>name</b> is missing' );
        }
        if(!isset($_POST['email'])){
            $this->_sendResponse(500, 'Error: Parameter <b>email</b> is missing' );
        }
        
        //add to the peple table first, need to get the auto incremented ID 
        //in order to add to emails table
        $person = new People();
        $person->name = $_POST['name'];
        
        if($person->validate()){
            $person->save();
            $person_id = $person->name_id;
        }
        else{
    		$this->_sendResponse(406, 'Person data not valid');
    	}
        
        //enter emails information, already validate email syntax in model
        //also check the emila is not already in the database
        $email_address = $_POST['email'];
        $ok = Emails::model()->checkEmail($email_address);
        if($ok){
            $details = new Emails();
            $details->name_id = $person_id;
            $details->email = $email_address;
            
            if($details->validate()){
                $details->save();
                $this->_sendResponse(200, CJSON::encode(array("People"=>$person, "emails"=>$details)));		

            }
            else{
                $this->_sendResponse(406, 'Person or email data not valid');
            }
            
        }
        else{
             $this->_sendResponse(406, 'Email address already in use');

        }
        
    }
    
    /*
     * Add an email address to an existing person
     */
    public function actionAddEmail(){
        if(!isset($_POST['name_id'])){
            $this->_sendResponse(500, 'Error: Parameter <b>name_id</b> is missing' );
        }
        
        if(!isset($_POST['email'])){
            $this->_sendResponse(500, 'Error: Parameter <b>email</b> is missing' );
        }
        
        $person_id = People::model()->findByPk($_POST['name_id']);
        $email_address = $_POST['email'];
        
        if(!empty($person_id)){
            $ok = Emails::model()->checkEmail($email_address);
            if($ok){
                $details = new Emails();
                $details->name_id = $person_id->name_id;
                $details->email = $email_address;

                if($details->validate()){
                    $details->save();
                    $this->_sendResponse(200, CJSON::encode(array("emails"=>$details)));		

                }
            }
            else{
                $this->_sendResponse(406, 'Email data not valid');
            }
            
        }
        else{
            $this->_sendResponse(406, 'Person data not valid, no records found.');
        }
    }
    
    
    
    /*
     * Edit a persons name
     */
    public function actionUpdatePerson()
    {
        $put_vars = array();
        parse_str(file_get_contents('php://input'), $put_vars);

        if(!isset($put_vars['name_id'])){
            $this->_sendResponse(500, 'Error: Parameter <b>name_id</b> is missing' );
        }
        
        if(!isset($put_vars['name'])){
            $this->_sendResponse(500, 'Error: Parameter <b>name</b> is missing' );
        }
        
        $person=People::model()->findByPk($put_vars['name_id']);
    	        
    	if(!empty($person))
    	{
                $old_name = $person->name;
          	$person->name = $put_vars['name'];
    		$person->save();
    		$this->_sendResponse(200, sprintf('<b>%s</b> updated to <b>%s</b>.', $old_name, $person->name) );
    	}
    	else 
    	{
    		$this->_sendResponse(406, 'Person data not valid');
    	}
    }
    
    /*
     * edit an email address
     */
    public function actionUpdateEmail(){
         $put_vars = array();
        parse_str(file_get_contents('php://input'), $put_vars);

                
        if(!isset($put_vars['old_email'])){
            $this->_sendResponse(500, 'Error: Parameter <b>old_email</b> is missing' );
        }
        
        if(!isset($put_vars['new_email'])){
            $this->_sendResponse(500, 'Error: Parameter <b>new_email</b> is missing' );
        }
        
    	$details=Emails::model()->findByAttributes(array('email'=>$put_vars['old_email']));

    	if(!empty($details))
    	{
          	$details->email = $put_vars['new_email'];
    		$details->save();
    		$this->_sendResponse(200, sprintf('<b>%s</b> updated to <b>%s</b>.', $put_vars['old_email'], $details->email) );
    	}
    	else 
    	{
    		$this->_sendResponse(406, 'Email data not valid');
    	}
        
    }
    
    
    
    
    /*
     * delete a person from the People table 
     * this will also delete all email addresses due to the 'on delete cascade' constraint
     * @person_id the id of the person to delete
     */
    public function actionDeletePerson()
    {
        $to_delete=People::model()->findByPk($_GET['name_id']);
        $name = $to_delete->name;
        
        if(!empty($to_delete))
    	{
	    $to_delete->delete();
	    $this->_sendResponse(200, sprintf('<b>%s</b> and their associated email addresses successfuly deleted',
                                $name) );
	    	
    	}
    	else 
    	{
    		$this->_sendResponse(406, sprintf('<b>%s</b> data not valid, unable to delete.', $name) );
    	}
        
    }
    
    /*
     * delete an email address - since email addresses are unique we can search by 
     * email address instead of using an ID
     */
    public function actionDeleteEmail()
    {
        $to_delete=Emails::model()->findByAttributes(array('email'=>$_GET['email']));
    	
    	if(!empty($to_delete))
    	{
	    $to_delete->delete();
	    $this->_sendResponse(200, sprintf('<b>%s</b deleted successfully', $_GET['email']));	
	    	
    	}
    	else 
    	{
    		$this->_sendResponse(406, sprintf('<b>%s</b data not valid, unable to delete.', $_GET['email']));
    	}
    }
    
    /*
     * *******************************************************************************
     *                           BELOW HERE IS TAKEN FROM 
     *       http://www.yiiframework.com/wiki/175/how-to-create-a-rest-api/#hh16
     * *******************************************************************************
     */
    
    /**
     * Sends the API response 
     * 
     * @param int $status 
     * @param string $body 
     * @param string $content_type 
     * @access private
     * @return void
     */
    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if($body != '')
        {
            // send the body
            echo $body;
        }
        // we need to create the body if none is passed
        else
        {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch($status)
            {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on 
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
    </head>
    <body>
        <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
        <p>' . $message . '</p>
        <hr />
        <address>' . $signature . '</address>
    </body>
    </html>';

            echo $body;
        }
        Yii::app()->end();
    }
    
    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
        );

        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    
}