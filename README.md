# Project Overview #

I created a new webapp using yiic, connected the external sql database, created the models files for the database tables 
using gii and finally created the api controller again using gii.

Some assumptions I made when creating the database were that name do not necessarily have to be unique, there could be 
two identical names in the address book. Email addresses however are unique. This is reflected in the database schema, 
the ‘People’ table contains ‘names’ and an auto increment ‘name id’ that serves as primary key. 
The ‘Emails’ table containing ‘email’ and a foregin key of the ‘name id’.

Unfortunately, after upgrading to Mavericks last week I discovered that my phpunit no longer worked and after spending far
too long attempting unsuccessfully to re install it I decided to move onto the api demo without unit tests. Ideally I would 
have written some tests for the api, for testing I used the chrome RESTclient postman.

Lastly, as reference for this I used the [yii tutorial](http://www.yiiframework.com/wiki/175/how-to-create-a-rest-api/#hh16) from it I took two functions to format the 
json response and get http error codes. All other functions are my own.

Hopefully you’ll find the project up to scratch!

***

#**API Documentation** #

Base Url         http://localhost/apiDemo/index.php/api/

+ List		
+ ViewPerson
+ ViewEmail
+ CreatePerson
+ AddEmail
+ UpdatePerson
+ UpdateEmail
+ DeletePerson
+ DeleteEmail

##List##
Returns the contents of either the People table or Emails table in json format.

HTTP GET request to baseUrl/list/model/\{table name\}

Required GET parameter:

Parameter     |||||||||||| Description
:-------------||||||||||||:---------------------------------------------------------------
model         |||||||||||| The table name you want to view, either ‘people’ or ‘emails’ 

##ViewPerson##
Returns an array of all the email addresses associated with a person

HTTP GET request to baseUrl/viewPerson?name\_id=\{valid name\_id\}

Required GET parameter:

Parameter     |||||||||||| Description
:-------------||||||||||||:---------------------
name\_id      |||||||||||| A valid entry from the name\_id column of the People table in the database

##ViewEmail##
Returns the person name and name\_id associated with an email address

HTTP GET request to baseUrl/viewEmail?email=\{valid email\}

Required GET parameter:

Parameter     |||||||||||| Description
:-------------||||||||||||:---------------------
email         |||||||||||| A valid entry from the email column of the Emails table in the database

##CreatePerson##
Create BOTH a new person and email address record. Also returns an array showing the newly created database rows

HTTP POST request to baseUrl/createPerson

Required POST parameters:

Parameter     |||||||||||| Description
:-------------||||||||||||:---------------------
name          |||||||||||| A string under 30 characters in length. Name’s don’t have to be unique
email         |||||||||||| An email address, this must be unique and the call will fail if there is an existing email in the database by the same name

##AddEmail##
Add email address for an existing person

HTTP POST request to baseUrl/addEmail

Required POST parameters:

Parameter     |||||||||||| Description
:-------------||||||||||||:---------------------
name\_id      |||||||||||| A valid entry from the name\_id column of the People table in the database
email         |||||||||||| Any email that follows standard email syntax


##UpdatePerson##
Edit the ‘name’ attribute for a given person.

HTTP PUT request to baseUrl/updatePerson

Required PUT parameters:

Parameter     |||||||||||| Description
:-------------||||||||||||:---------------------
name\_id      |||||||||||| A valid entry from the name\_id column of the People table in the database
name          |||||||||||| The value that name will be updated to


##UpdateEmail##
Edit the ‘email’ attribute for a given person.

HTTP PUT request to baseUrl/updateEmail

Required PUT parameters:

Parameter     |||||||||||| Description
:-------------||||||||||||:---------------------
old\_email    |||||||||||| A valid entry from the email column of the Emails table in the database
new\_email    |||||||||||| The value that email will be updated to


##DeletePerson##
Delete a person from the address book, in doing so delete all of their associated email addresses

HTTP DELETE request to baseUrl/deleteEmail?name\_id=\{valid name\_id\}

Required GET parameter:

Parameter     |||||||||||| Description
:-------------||||||||||||:---------------------
name\_id      |||||||||||| A valid entry from the name\_id column of the People table in the database
	

##DeleteEmail##
Delete an email address from the address book, this doesn’t affect the user conneted with the email.

HTTP DELETE request to baseUrl/deleteEmail?email=\{valid email\}

Required GET parameter:

Parameter     |||||||||||| Description
:-------------||||||||||||:---------------------
email         |||||||||||| A valid entry from the email column of the Emails table in the database
	
